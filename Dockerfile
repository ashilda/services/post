FROM anapsix/alpine-java:8

EXPOSE 4568

ADD target/post-jar-with-dependencies.jar /post-jar-with-dependencies.jar

ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,address=8568,suspend=n", "-jar", "/post-jar-with-dependencies.jar"]