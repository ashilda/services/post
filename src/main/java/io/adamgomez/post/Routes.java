package io.adamgomez.post;

import io.adamgomez.post.models.Post;
import io.adamgomez.post.models.Posts;
import io.adamgomez.common.services.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import spark.Response;
import spark.SparkBase;


import static spark.Spark.post;
import static spark.Spark.get;

public class Routes {

    public static void main(String[] args) {

        SparkBase.setPort(4568);

        post("/post", (req, res) -> {

            setHeaders(res);

            JSONObject jsonObject = parseJson(req.body());
            String userId = validateRequest(jsonObject);

            if (userId.equals("")) {
                res.status(500);
                return "";
            }

            DBConnection connection = new DBConnection();

            jsonObject = (JSONObject)jsonObject.get("post");

            Post post = new Post(userId, (String)jsonObject.get("text"));
            if (post.insertIntoDatabase(connection)) {
                connection.close();
                res.status(200);
                return "";
            } else {
                connection.close();
                res.status(500);
                return "";
            }
        });

        get("/post", (req, res) -> {

            String token = req.queryParams("token");

            DBConnection connection = new DBConnection();

            Posts posts = new Posts(connection);
            connection.close();
            return posts.getPostsJSON();
        });

    }

    private static JSONObject parseJson(String body) {
        JSONParser parser = new JSONParser();
        try {
            return (JSONObject)parser.parse(body);
        } catch(ParseException pe) {
            System.out.println("Could not parse request");
            return null;
        }
    }

    private static String validateRequest(JSONObject jsonObject) {

        if (jsonObject == null) {
            return "Error: parsing JSON";
        }
        jsonObject = (JSONObject)jsonObject.get("post");
        String token = (String)jsonObject.get("token");
        String responseJson = RequestValidator.execute(token);

        JSONObject userJson = parseJson(responseJson);
        userJson = (JSONObject)userJson.get("user");
        return (String)userJson.get("id");
    }

    private static void setHeaders(Response res) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "POST");
    }

}
