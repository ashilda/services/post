package io.adamgomez.post.models;

import io.adamgomez.common.services.DBConnection;
import io.adamgomez.common.utils.StatementGenerator;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Posts {

    private static String DATE_PATTERN = "dd/MM/YYYY HH:mm";

    private ArrayList<Post> posts;

    public Posts(DBConnection connection) {
        this.posts = new ArrayList<>();
        try {
            ResultSet rs = connection.executeQuery(generateGetPostsStatement());
            while(rs.next()) {
                this.posts.add(new Post(rs.getString("first_name"), rs.getString("last_name"), rs.getString("text"), rs.getString("date")));
            }
        } catch(SQLException sqle) {
            System.out.println("Error");
        }
    }

    public String generateGetPostsStatement() {
        ArrayList<String> columns = new ArrayList<>();
        ArrayList<String> conditions = new ArrayList<>();
        columns.add("first_name");
        columns.add("last_name");
        columns.add("text");
        columns.add("date");
        String selectStatement = StatementGenerator.select(columns, "post", conditions);
        conditions.add("post.user_id=user.id");
        return StatementGenerator.join("user", selectStatement, conditions);
    }

    public String getPostsJSON() {
        Collections.sort(this.posts);
        StringBuffer sb = new StringBuffer();
        sb.append("{ \"posts\": [");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
        for (int i = 0; i < this.posts.size(); i++) {
            String date = simpleDateFormat.format(new Date(Long.parseLong(this.posts.get(i).getDate())));
            sb.append("{ \"first_name\": \"" + this.posts.get(i).getFirstName() + "\", ");
            sb.append("\"last_name\": \"" + this.posts.get(i).getLastName() + "\", ");
            sb.append("\"text\": \"" + this.posts.get(i).getText() + "\", ");
            sb.append("\"date\": \"" + date + "\" }");
            if (i < (this.posts.size()-1)) {
                sb.append(", ");
            }
        }
        sb.append("] }");
        return sb.toString();
    }
}
