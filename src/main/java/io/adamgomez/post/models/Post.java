package io.adamgomez.post.models;

import io.adamgomez.common.services.DBConnection;
import io.adamgomez.common.utils.StatementGenerator;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Post implements Comparable<Post> {

    private String id;
    private String userId;
    private String firstName;
    private String lastName;
    private String text;
    private String date;

    private static String POST = "post";
    private static String ID = "id";
    private static String USERID = "user_id";
    private static String TEXT = "text";
    private static String DATE = "date";

    public Post(String userId, String text) {
        this.userId = userId;
        this.text = text;
    }

    public Post(String firstName, String lastName, String text, String date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.text = text;
        this.date = date;
    }

    public boolean insertIntoDatabase(DBConnection connection) {
        this.date = Long.toString(System.currentTimeMillis());
        if ((this.userId == null) || (this.text == null) || (this.date == null)) {
            return false;
        } else {
            ResultSet rs = connection.executeQuery(generateInsertPostStatement());
            return true;
        }
    }

    public String generateInsertPostStatement() {
        ArrayList<String> columns = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();
        columns.add(USERID);
        columns.add(TEXT);
        columns.add(DATE);
        values.add("\"" + this.userId + "\"");
        values.add("\"" + this.text + "\"");
        values.add("\"" + this.date + "\"");
        return StatementGenerator.insert(POST, columns, values);
    }

    @Override
    public int compareTo(Post post) {
        return post.getDate().compareTo(this.getDate());
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getText() {
        return this.text;
    }

    public String getDate() {
        return this.date;
    }
}
